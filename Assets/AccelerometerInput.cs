﻿using UnityEngine;
using System.Collections;

public class AccelerometerInput : MonoBehaviour {
    //THey do not apply the low pass filter, just define it. Alternately the scripts assumes the device will run at 60fps. Instead it would be better to set the values based on current UpdateRate. Or call the function in FixedUpdate()


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Input.acceleration.x, 0, -Input.acceleration.z);
        if(Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, 1, 0f);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0, -1, 0f);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-1, 0, 0f);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(1, 0, 0f);
        }
    }
}
