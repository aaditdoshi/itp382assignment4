﻿using UnityEngine;
using System.Collections;

public class PinchZoom : MonoBehaviour {

    public float orthozoomspeed= .5f;
    public float perspectivezoomspeed = .5f;

    void Update()
    {

        float deltamag = 0.0f;

        if (Input.touchCount ==2)
        {
            Touch touch0 = Input.GetTouch(0);
            Touch touch1 = Input.GetTouch(1);

            Vector2 touch0Prevpos = touch0.position - touch0.deltaPosition;
            Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;

            //Magnitude of previous 
            float prevTouchDeltaMag = (touch0Prevpos - touch1PrevPos).magnitude;
            float touchdeltaMag = (touch0.position - touch1.position).magnitude;


          deltamag = prevTouchDeltaMag - touchdeltaMag;

            if(Camera.main.orthographic)
            {
                Camera.main.orthographicSize += deltamag * orthozoomspeed;
                Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
            }
            else
            {
                Camera.main.fieldOfView += deltamag * perspectivezoomspeed;
                Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 0.1f, 179.9f);
            }
        }
        else
        {
     deltamag = Input.mouseScrollDelta.y;

            if (Camera.main.orthographic)
            {
                Camera.main.orthographicSize += deltamag * orthozoomspeed;
                Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
            }
            else
            {
                Camera.main.fieldOfView += deltamag * perspectivezoomspeed;
                Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 0.1f, 179.9f);
            }
        }
    }
}
