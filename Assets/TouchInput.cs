﻿using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour {

	// Use this for initialization
	void Start () {
	if(Input.multiTouchEnabled)
        {
            Debug.Log("Multi-Touch is Enabled");
        }
        else
        {
            Debug.Log("Multi-Touch is not Enabled");
        }
	}
	
	// Update is called once per frame
	void Update () {



        int touchCount = Input.touchCount;
        if (touchCount>0)
        {
            for(int i = 0; i< touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);

                if(touch.phase == TouchPhase.Began)
                {
                    RayCastDestroy(touch.position);
                }

            }


            
        }
        else if(Input.GetMouseButtonDown(0))
        {

            RayCastDestroy(Input.mousePosition);
        }
	}

    void RayCastDestroy(Vector3 Position)
    {
        Ray screenRay = Camera.main.ScreenPointToRay(Position);
        RaycastHit hit;
        if (Physics.Raycast(screenRay, out hit))
        {
            Destroy(hit.collider.gameObject);
        }
    }
}
